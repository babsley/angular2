import { Component, OnInit, OnDestroy } from '@angular/core';
import { Product } from '../shared/models/product.model';
import { ProductsService } from '../shared/services/products.service';
import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from 'ng2-translate';

@Component({
    moduleId: module.id,
    selector: 'products',
    templateUrl: 'products.html',
    styleUrls: ['products.css'],
})
export class ProductsComponent implements OnInit, OnDestroy {
    private translateSubscription: Subscription;
    private productSubscription: Subscription;
    private pending: boolean;
    private count: number = 10;
    private page: number = 1;
    private total: number;

    products: Product[];
    errorMessage: any;

    constructor(private productsServices: ProductsService, private translateService: TranslateService) {
        this.translateSubscription = this.translateService.onLangChange.subscribe(() => this.getProducts());
    }

    pageChange(e) {
        console.log(e);
    };

    getProducts() {
        this.pending = true;
        this.productSubscription = this.productsServices
            .getProducts(this.translateService.currentLang).subscribe(
                (data) => {
                    this.products = data.products || [];
                    this.total = data.count;
                    this.pending = false;
                },
                (error) => {
                    this.pending = false;
                    this.errorMessage = <any>error;
                }
            );
    }

    ngOnInit(): void {
        this.getProducts();
    }

    ngOnDestroy(): void {
        this.productSubscription.unsubscribe();
        this.translateSubscription.unsubscribe();
    }
}
