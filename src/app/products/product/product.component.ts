import { Component, Input } from '@angular/core';
import { ShoppingCartService } from '../../shared/services/shopping-cart.service';
import { Product } from '../../shared/models/product.model';

@Component({
    moduleId: module.id,
    selector: 'app-product',
    templateUrl: 'product.html',
    styleUrls: ['product.css'],
})
export class ProductComponent {
    @Input() product: Product;

    constructor(private shoppingCartService: ShoppingCartService) {
    }

    addToCart(product): void {
        this.shoppingCartService.addProduct(product);
    }
}
