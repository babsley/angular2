import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { Http } from '@angular/http';
import { FormsModule }   from '@angular/forms';
import { AlertModule, DropdownModule, PaginationModule } from 'ng2-bootstrap';
import { TranslateModule, TranslateLoader, TranslateStaticLoader } from 'ng2-translate';
import { CookieService } from 'angular2-cookie/services/cookies.service';


import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { ProductsComponent } from './products/products.component';
import { ProductComponent } from './products/product/product.component';
import { AboutComponent } from './about/about.component';
import { PopupComponent } from './popup/popup.component';
import { ShoppingCartComponent } from './shopping-cart/shopping-cart.component';
import { BooleanPropFilterPipe } from './shared/pipes/boolean-prop-filter.pipe';
import { ShoppingCartService } from './shared/services/shopping-cart.service';
import { ProductsService } from './shared/services/products.service';
import { APP_ROUTES } from './app.routes';

@NgModule({
    imports: [
        RouterModule.forRoot(APP_ROUTES),
        BrowserModule,
        HttpModule,
        FormsModule,
        DropdownModule.forRoot(),
        AlertModule.forRoot(),
        PaginationModule.forRoot(),
        TranslateModule.forRoot({
            provide: TranslateLoader,
            useFactory: (http: Http) => new TranslateStaticLoader(http, 'assets/i18n', '.json'),
            deps: [Http]
        })
    ],
    declarations: [
        AppComponent,
        HeaderComponent,
        HomeComponent,
        AboutComponent,
        ProductsComponent,
        ShoppingCartComponent,
        ProductComponent,
        BooleanPropFilterPipe,
        PopupComponent
    ],
    providers: [
        ShoppingCartService,
        CookieService,
        ProductsService
    ],
    bootstrap: [AppComponent]
})

export class AppModule {
}
