import { Component, ElementRef, ViewChild, OnInit } from '@angular/core';
import { ShoppingCartService } from '../shared/services/shopping-cart.service';
import { CartProduct } from '../shared/models/cart-product.model';
import { PopupComponent } from '../popup/popup.component';
import 'rxjs/add/operator/debounceTime';


@Component({
    moduleId: module.id,
    selector: 'app-cart',
    templateUrl: 'shopping-cart.html',
    styleUrls: ['shopping-cart.css']
})
export class ShoppingCartComponent implements OnInit {
    @ViewChild(PopupComponent) popup: PopupComponent;
    cartProducts: CartProduct[];
    totalPrice: number = null;
    pending: boolean = false;

    constructor(private shoppingCartService: ShoppingCartService, private elementRef: ElementRef) {
    }

    public checkout(): void {
        let alert: any = {
            text: 'cart.checkoutMsg',
            type: 'danger'
        };

        this.popup.doShow(alert);
    }

    public isPending() {
        return this.shoppingCartService.pending;
    }

    public getProducts(): void {
        this.cartProducts = this.shoppingCartService.getProducts();
    }

    public onQuantityChanged(cartProduct) {
        let element = this.elementRef.nativeElement.querySelector('#quantity' + cartProduct.product.id);
        element.value = cartProduct.quantity;
        this.totalPrice = this.shoppingCartService.getTotalPrice(true);
    }

    public removeCartProduct(cartProduct) {
        this.shoppingCartService.removeProduct(cartProduct);
        this.getProducts();
        this.totalPrice = this.shoppingCartService.getTotalPrice(true);
    }

    ngOnInit(): void {
        this.getProducts();
        this.totalPrice = this.shoppingCartService.getTotalPrice(true);
    }
}
