import { Component } from '@angular/core';
import { Popup } from '../shared/models/popup.model';

@Component({
    moduleId: module.id,
    selector: 'popup',
    templateUrl: 'popup.html'
})
export class PopupComponent {
    private alerts: Popup[] = [];
    private uniqueId: number = 0;

    public doShow(popup: Popup): void {
        popup.id = this.uniqueId;
        this.alerts.push(new Popup(popup));

        this.uniqueId++;
    }

    public close(popup) {
        this.alerts = this.alerts.filter((alert) => {
            if (popup.id === alert.id) {
                return false;
            }

            return true;
        });
    }
}
