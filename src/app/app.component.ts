import { Component, ViewChild, OnInit, OnDestroy } from '@angular/core';
import { PopupComponent } from './popup/popup.component';
import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from 'ng2-translate';
import { CookieService } from 'angular2-cookie/core';

@Component({
    moduleId: module.id,
    selector: 'app',
    templateUrl: 'app.html',
    styleUrls: [
        'app.component.css'
    ]
})
export class AppComponent implements OnInit, OnDestroy {
    @ViewChild(PopupComponent) popup: PopupComponent;
    private translateSubscription: Subscription;
    alert: any = {text: 'app_message'};


    constructor(private translateService: TranslateService, private cookieService: CookieService) {
    }

    private setLanguage(): void {
        const langs = ['en', 'ru'];
        let cookieLang = this.cookieService.get('lang');

        this.translateService.addLangs(langs);

        if (cookieLang && langs.includes(cookieLang)) {
            this.translateService.use(cookieLang);
        } else {
            let browserLang = this.translateService.getBrowserLang();
            this.translateService.use(browserLang.match(/en|ru/) ? browserLang : 'en');
        }

        this.translateSubscription = this.translateService.onLangChange.subscribe((lang) => {
            this.cookieService.put('lang', lang.lang);
        });
    }

    ngOnInit() {
        this.setLanguage();
        this.popup.doShow(this.alert);
    }

    ngOnDestroy() {
        this.translateSubscription.unsubscribe();
    }
}
