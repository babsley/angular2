import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { ShoppingCartService } from '../shared/services/shopping-cart.service';
import { TranslateService } from 'ng2-translate';

@Component({
    moduleId: module.id,
    selector: 'app-header',
    templateUrl: 'header.html',
    styleUrls: [
        'header.css'
    ]
})

export class HeaderComponent implements OnDestroy {
    private cartQuantity: number;
    private subscription: Subscription;

    constructor(private shoppingCartService: ShoppingCartService, private translateService: TranslateService) {
        this.cartQuantity = this.shoppingCartService.getTotalQuantity();

        this.subscription = this.shoppingCartService
            .getProductsQuantitySubscription()
            .subscribe((quantity) => {
                this.cartQuantity = quantity;
            });
    }

    changeLanguage(lang) {
        if (lang !== this.translateService.currentLang) {
            this.translateService.use(lang);
        }
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
