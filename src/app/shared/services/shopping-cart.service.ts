import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';
import { Product } from '../models/product.model';
import { CartProduct } from '../models/cart-product.model';
import { ProductsService } from '../services/products.service';
import { Subscription } from 'rxjs/Subscription';
import { TranslateService } from 'ng2-translate';


@Injectable()
export class ShoppingCartService {
    static readonly CART_DATA_KEY = 'cartProducts';
    private translateSubscription: Subscription;
    private totalPrice: number = 0;
    private updateProductQuantity$ = new Subject<any>();
    private cartProducts: CartProduct[] = [];
    private productsQuantity: number = 0;
    public pending: boolean = false;

    constructor(private translateService: TranslateService, private productsService: ProductsService) {
        this.restoreDataFromLocalStorage();
        this.translateSubscription = this.translateService.onLangChange.subscribe(data => this.renewProducts(data.lang));
    }

    private renewProducts(lang) {
        if (!this.cartProducts.length) {
            return;
        }

        this.pending = true;

        this.productsService.getProducts(lang,
            this.cartProducts.map(item => item.product.id).toString(),
            'id,name,description')
            .subscribe(data => {
                data.products.forEach(translateItem => {
                    let findProduct = this.cartProducts.find(item => item.product.id === translateItem.id);

                    if (findProduct) {
                        for (let key in translateItem) {
                            if (translateItem.hasOwnProperty(key)) {
                                findProduct.product[key] = translateItem[key];
                            }
                        }
                    }
                });

                this.pending = false;
                this.saveDataToLocalStorage();
            });
    }

    private processCartChanges() {
        this.productsQuantity = this.getTotalQuantity();
        this.totalPrice = this.getTotalPrice();
        this.updateProductQuantity$.next(this.productsQuantity);
        this.saveDataToLocalStorage();
    }

    private saveDataToLocalStorage() {
        localStorage.setItem(ShoppingCartService.CART_DATA_KEY,
            JSON.stringify(this.cartProducts.map(
                (value) => ({product: value.product, quantity: value.quantity}))
            ));
    }

    private restoreDataFromLocalStorage(): void {
        let restoreData = JSON.parse(localStorage.getItem(ShoppingCartService.CART_DATA_KEY)) || [];

        restoreData.forEach((cartProduct) => {
            this.addNewCartProduct(cartProduct.product, cartProduct.quantity);
        });
    }

    public getTotalQuantity(doNotCalculate = false) {
        if (!this.cartProducts.length) {
            return 0;
        }

        if (doNotCalculate) {
            return this.productsQuantity;
        }

        return this.cartProducts.reduce(function (sum, current) {
            return sum + current.quantity;
        }, 0);
    }

    public getProductsQuantitySubscription(): Observable<any> {
        return this.updateProductQuantity$.asObservable();
    };

    private addNewCartProduct(product: Product, quantity = 1) {
        let cartProduct = new CartProduct(product, quantity);
        this.cartProducts.push(cartProduct);

        cartProduct.quantitySubscription = cartProduct.quantity$.subscribe(() => {
            this.processCartChanges();
        });
    }

    public addProduct(product: Product) {
        let findElement: CartProduct = this.cartProducts.find((elem) => elem.product.id === product.id);

        if (!findElement) {
            this.addNewCartProduct(product);
        } else {
            ++findElement.quantity;
        }
    }

    public removeProduct(cartProduct: CartProduct) {
        this.cartProducts = this.cartProducts.filter((elem: any) => {
            return elem.product.id !== cartProduct.product.id;
        });

        cartProduct.quantitySubscription.unsubscribe();
        this.processCartChanges();
    }

    public getProducts() {
        return this.cartProducts;
    }

    public getTotalPrice(doNotCalculate = false) {
        if (doNotCalculate) {
            return this.totalPrice;
        }

        return this.cartProducts.reduce(function (sum, current) {
            return sum + current.quantity * current.product.price;
        }, 0);
    }
}
