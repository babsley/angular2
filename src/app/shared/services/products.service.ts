import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class ProductsService {
    private url = 'api/products?';

    constructor(private http: Http) {
    }

    getProducts(lang: string, idList?: any, localize?: any): Observable<any> {
        lang = 'lang=' + lang || 'en';
        idList = idList ? '&idList=' + idList : '';
        localize = localize ? '&localize=' + localize : '';

        return this.http.get(this.url + lang + idList + localize)
            .map(this.extractData)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.data || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;

        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }

        return Observable.throw(errMsg);
    }
}
