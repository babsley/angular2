import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Product } from './product.model';


export class CartProduct {
    product: Product;
    private _quantitySource = new BehaviorSubject < number >(0);
    public quantity$ = this._quantitySource.asObservable();
    public quantitySubscription;

    constructor(product: Product, quantity: number) {
        this.product = product;
        this.quantity = quantity;
    }

    public set quantity(value: number) {
        if (value > this.product.quantity) {
            value = this.product.quantity;
        } else if (!value || value < 0) {
            value = 0;
        }

        this._quantitySource.next(value);
    }

    public get quantity() {
        return this._quantitySource.getValue();
    }
}
