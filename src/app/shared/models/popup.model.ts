export class Popup {
    id: number;
    text: string;
    type?: string;
    dismissible?: boolean = true;
    delay?: number = 2500;

    constructor(params: Popup) {
        let types: string[] = ['info', 'success', 'warning', 'danger'];
        let allowType = types.find(type => type === params.type);

        params.type = allowType ? allowType : types[0];

        Object.assign(this, params);
    }
}
