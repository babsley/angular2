import { Pipe, PipeTransform } from '@angular/core';

@Pipe({name: 'BooleanPropFilter'})
export class BooleanPropFilterPipe implements PipeTransform {
    transform(arr: any[], property: string) {
        if (!property || !arr) {
            return;
        }

        return arr.filter((item) => {
            return item[property];
        });
    }
}
