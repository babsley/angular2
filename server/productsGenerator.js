const faker = require('faker');
const currentPath = require('path').dirname(require.main.filename);
const fileName = 'mock.products.';
const langList = ['ru', 'en'];
const fileExtension = '.json';
const fs = require('fs');

function generateProduct(id, data) {
    let price = faker.commerce.price()
    let quantity = faker.random.number(100);
    let image = faker.random.image(400, 250);
    let available = quantity > 0 ? true : false;

    langList.forEach((lang) => {
        data[lang] = data[lang] || [];
        let product = {};

        faker.locale = lang;
        product.id = id;
        product.price = price;
        product.quantity = quantity;
        product.image = image;
        product.available = available;
        product.name = faker.commerce.product();
        product.description = faker.commerce.productAdjective() + ' ' + faker.commerce.productMaterial() + ' ' + product.name;
        data[lang].push(product);
    });
};

function generateFiles(productCounts) {
    let data = {};

    for (let i = 1; i <= productCounts; i++) {
        generateProduct(i, data);
    }

    langList.forEach((lang) => {
        let obj = {
            products: data[lang],
            count: productCounts
        }

        fs.writeFile(currentPath + '/' + fileName + lang + fileExtension, JSON.stringify(obj));
    });
}

generateFiles(250);