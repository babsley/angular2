const express = require('express');
const app = express();
const langList = ['ru', 'en'];

app.get('/api/test', function (req, res) {
    res.send('API server works!')
});


app.get('/api/products', (req, res) => {
        let data = req.query;
        const mockProducts = require(`./mock.products.${data.lang && langList.includes(data.lang) ? data.lang : 'en'}.json`);
        let products = mockProducts.products;

        if (data.idList && mockProducts) {
            products = products.filter(product => data.idList.includes(product.id));
        }

        if (data.localize) {
            if (data.localize.indexOf('id') < 0) {
                data.localize = data.localize + ',id';
            }

            data.localize = data.localize.split(',');

            products = products.map((product) => {
                let result = {};
                for (let key in product) {
                    if (product.hasOwnProperty(key)) {
                        if (data.localize.indexOf(key) !== -1) {
                            result[key] = product[key];
                        }
                    }
                }
                return result;
            });
        }

        setTimeout(() => {
            res.send({
                data: {
                    products: products,
                    count: mockProducts.count
                }
            });
        }, 500);
    }
);

app.listen(3003, function () {
    console.log('server port 3003')
});
